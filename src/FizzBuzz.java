
public class FizzBuzz {
    public static void main(String[] args) {
        System.out.println("For Loop Solution");

        for (int i = 1; i <= 100; i++) {
            if (i % 15 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(String.valueOf(i));
            }


        }
            System.out.println("");
            System.out.println("Recursive Solution");
            RecursiveSolution(1);


        }

        private static void RecursiveSolution (int n){
            String s = "";
            if (n > 100)
                return;

            if (n % 15 == 0) {
                System.out.println("FizzBuzz");
            } else if (n % 3 == 0) {
                System.out.println("Fizz");
            } else if (n % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(String.valueOf(n));
            }

                RecursiveSolution(n + 1);
            }

        }










