public class Incapsulation {
    public static void main(String[] args) {
        Person person1 = new Person();
        person1.setName("");
        person1.setAge(-12);
        System.out.println(person1.getName());
        System.out.println(person1.getAge());




        }
}
class Person {
    private String name;
    private int age;

    public void setName(String userName) {
        if (userName.isEmpty()) {
            System.out.println("Please enter your name");
        } else {
            name = userName; }
        }
        public String getName () {
            return name;
        }

        public void setAge ( int userAge){
            if (userAge < 0) {
                System.out.println("Age cannot be negative");
            } else {

                age = userAge; }
            }

            public int getAge() {
                return age;
            }


            int calculateYearsToRetairement () {
                int years = 65 - age;
                return years;
            }


            void speak () {
                System.out.println("My name is " + name + ", I am " + age + " years old");
            }

            void sayHello () {
                System.out.println("Hello!");
            }

        }
