
//public class If {
//    public static void main(String[] args) {
//        if (5>7) {
//            System.out.println("Yes, true");
//        } else {
//            System.out.println("No, not true");
//        }
//    }
//}




public class If {
    public static void main(String[] args) {
        int myInt = 15;
        if (myInt < 14) {
            System.out.println("Yes, true");
        } else if (myInt > 16){
            System.out.println("No, not true");
        } else {
            System.out.println("None of the cases above is true");
        }
    }
}
